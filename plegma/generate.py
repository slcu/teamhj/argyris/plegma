import os.path  # type: ignore
from scipy.ndimage.morphology import generate_binary_structure  # type: ignore
from scipy.ndimage.filters import gaussian_filter, median_filter  # type: ignore
from skimage.morphology import binary_erosion  # type: ignore
from skimage.segmentation import morphological_chan_vese  # type: ignore
from skimage import measure  # type: ignore
import tifffile as tiff  # type: ignore
import pyvista as pv  # type: ignore
import numpy as np  # type: ignore
import plegma.mesh as mp
import plegma.simplify as simpl


def read_image(fseg, res=[1, 1, 1]):
    f = tiff.TiffFile(fseg)
    seg = f.asarray().astype(int)

    # data = np.multiply(seg, res)

    return seg


def smooth_data(data, res=[1, 1, 1], niter=1):
    for _ in range(niter):
        data = median_filter(data, footprint=generate_binary_structure(3, 2))

    for _ in range(3):
        data = gaussian_filter(
            data, sigma=[2. * .25 / res[0],
                         2. * .25 / res[1],
                         2. * .25 / res[2]])

    return data


def get_contour(data):
    from scipy.ndimage import generate_binary_structure  # type: ignore

    factor = .5
    contour = morphological_chan_vese(data, iterations=10,
                                      init_level_set=data > factor * np.mean(data),
                                      smoothing=1, lambda1=1, lambda2=1)
    contour = binary_erosion(contour, generate_binary_structure(3, 2))
    contour = mp.fill_contour(contour, fill_xy=False)
    contour = binary_erosion(contour, generate_binary_structure(3, 2))

    return contour


def mk_mesh(contour, level=0, res=[1, 1, 1]):
    verts, faces, _, _ = measure.marching_cubes_lewiner(
        contour, level, spacing=list(res / np.min(res)), step_size=1,
        allow_degenerate=False)

    faces = np.hstack(np.c_[np.full(faces.shape[0], 3), faces])
    verts = np.multiply(verts, np.array(res))
    mesh = pv.PolyData(verts, faces)

    return mesh


def mk_surface(mesh):
    cut_off = 2

    mesh = mesh.clip('-x', origin=[mesh.points[:, 0].min() + cut_off, 0, 0])
    mesh = mesh.clip('-y', origin=[0, mesh.points[:, 1].min() + cut_off, 0])
    mesh = mesh.clip('-z', origin=[0, 0, mesh.points[:, 1].min() + cut_off])

    # mesh = mp.correct_bad_mesh(mesh)
    mesh = mp.ECFT(mesh, 100)
    mesh = mesh.compute_normals(inplace=False)

    return mesh


def fix_mesh(mesh):
    # mesh = mp.correct_bad_mesh(mesh)
    mesh = mp.ECFT(mesh, 0)

    mesh = mesh.compute_normals(inplace=False)

    if np.sum(mesh.point_normals, axis=0)[0] < 0:
        mesh.flip_normals()

    return mesh


def get_mesh_from_file(fin, npoints):
    (root, _) = os.path.splitext(fin)

    mesh_fn = root + "_auto.ply"

    data = read_image(fin)
    data = smooth_data(data)
    contour = get_contour(data)
    print("got contour")

    ctr = contour.astype(np.uint8)

    mesh = mk_mesh(ctr)
    #mesh = mk_surface(mesh)
    mesh = fix_mesh(mesh)

    mesh = simpl.remesh(mesh, npoints)
    mesh = mesh.smooth(n_iter=20,
                       convergence=0.0, edge_angle=15,
                       feature_angle=30, boundary_smoothing=True,
                       feature_smoothing=False, inplace=False)
    mesh = fix_mesh(mesh)

    mesh.save(mesh_fn, binary=False)

    return mesh


def get_mesh_from_segfile(fin, npoints, res=[1, 1, 1]):
    (root, _) = os.path.splitext(fin)

    mesh_fn = root + "_auto.ply"

    data = read_image(fin)
    data = smooth_data(data)

    mesh = mk_mesh(data, level=1, res=res)
    mesh = fix_mesh(mesh)

    mesh = simpl.remesh(mesh, npoints)
    mesh = mesh.smooth(n_iter=20,
                       convergence=0.0, edge_angle=15,
                       feature_angle=30, boundary_smoothing=True,
                       feature_smoothing=False, inplace=False)
#    mesh = fix_mesh(mesh)

    mesh.save(mesh_fn, binary=False)

    return mesh
