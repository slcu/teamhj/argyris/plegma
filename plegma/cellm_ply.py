import networkx as nx
from networkx.algorithms.cycles import cycle_basis
from itertools import combinations

def get_walls(v_coords, edges, vcells):
    from scipy.spatial.distance import euclidean

    walls = list()
    for e in list(edges):
        v1, v2 = e

        if (v1 not in vcells) or (v2 not in vcells):
            continue
        
        cs_v1 = set(vcells[v1])
        cs_v2 = set(vcells[v2])

        cs = cs_v1.intersection(cs_v2)


        v1_coords = v_coords[v1, :]
        v2_coords = v_coords[v2, :]

        if len(cs) == 2:
            walls.append(list(cs) + [v1, v2, euclidean(v1_coords, v2_coords)])

    return walls


def is_monocyclic(es):
    G = nx.Graph()
    G.add_edges_from(es)

    return len(cycle_basis(G)) == 1


def sort_vs(es):
    G = nx.Graph()
    G.add_edges_from(es)

    return cycle_basis(G)[0]


def to_ply(v_coords, edges, vcells):
    from collections import defaultdict

    ply_repr = """ply
format ascii 1.0
element vertex {nvs}
property float x
property float y
property float z
element face {ncells}
property list uchar int vertex_index
end_header
{vertex_repr}
{cells_repr}
"""
    ws = get_walls(v_coords, edges, vcells)

    d = defaultdict(list)
    for w in ws:
        if len(w) == 5:
            c1, c2, v1, v2, _ = w
            d[c1].append((v1, v2))
            d[c2].append((v1, v2))

    d_ = {k: sort_vs(ws[::]) for k, ws in d.items() if is_monocyclic(ws)}

    point_repr = "\n".join([" ".join([str(v[0]), str(v[1]), str(v[2])])
                            for v in list(v_coords)])

    cmap = dict()
    cells_reprs = list()
    for i, (k, vs) in enumerate(d_.items()):
        crepr = " ".join([str(len(vs))] + [str(v) for v in vs])
        cmap[i] = k
        cells_reprs.append(crepr)

    cells_repr = "\n".join(cells_reprs)

    return (ply_repr.format(nvs=str(len(v_coords)),
                            ncells=str(len(d_.keys())),
                            vertex_repr=point_repr,
                            cells_repr=cells_repr), cmap)
