import common.seg as seg
from common.seg import STissue, Cell
from typing import List, Dict, Union, Callable
import numpy as np  # type:ignore
from scipy.spatial import KDTree  # type: ignore
from scipy.linalg import lstsq  # type: ignore
from common.edict import union
from pyvista import PolyData


Num = Union[int, float]


T = np.array([[0.0856094, -0.0939332, -0.692672, 93.5559],
              [0.22725, 0.66368, -0.0619151, -5.33033],
              [0.661041, -0.215992, 0.110991, 18.824],
              [0, 0, 0, 1]])


T_ = np.array([[0.0379092, 0.141891, 0.590097, 3.17311],
               [0.579792, -0.183249, 0.00681576, 41.9055],
               [0.179414, 0.562203, -0.14671, -1.99739],
               [0, 0, 0, 1]])


T__ = np.array([[0.00744862, 0.827294, -0.0456243, 72.7435],
                [-0.244926 ,0.0413886 ,0.790475 ,30.1943] ,
                [0.791522 ,0.0205924 ,0.244172 ,83.3449] ,
                [0 ,0 ,0 ,1]])


def read_csv(fn: str) -> List[Dict[str, Num]]:
    def f_key(k, v):
        if k == "id":
            return int(v)
        else:
            return float(v)

    with open(fn) as fout:
        header = fout.readline().strip().split()

        cs = list()
        for ln in fout:
            elems = [el.strip() for el in ln.split()]
            cs.append(dict([(k, f_key(k, v)) for k, v in zip(header, elems)]))

    return cs


def from_csv(csv_f: str) -> STissue:
    d = read_csv(csv_f)
    base_ks = set(["x", "y", "z", "vol", "id"])

    cells = dict()
    for i, elem in enumerate(d):
        prop_ks = [k for k in set(elem.keys()).difference(base_ks)]
        cid = elem.get("id", i)
        pos = seg.Vec3(elem["x"], elem["y"], elem["z"])
        vol = elem.get("vol", 0.0)
        props = {k: elem[k] for k in set(prop_ks)}
        c = seg.Cell(cid, pos, vol, props)
        c.geneNms = prop_ks
        cells[cid] = c

    return seg.STissue(cells, dict(), geneNms=prop_ks)


def tr_ts(ts: STissue, T):
    for c in ts:
        cpos_ar = np.asmatrix(c.pos.toArray())
        npos_ar = np.squeeze(fh(np.matmul(T, th(cpos_ar).T).T))

        c.pos.x = npos_ar[0]
        c.pos.y = npos_ar[1]
        c.pos.z = npos_ar[2]


def idx1(x): return x[0]


def idx2(x): return x[1]


def dist(x): return x[2]


def get_points(ts):
    return np.array([[c.pos.x, c.pos.y, c.pos.z] for c in ts])


def map_points(source, target):
    n = len(source)
    tt = KDTree(target)

    ds, idxs = tt.query(source)

    return zip(list(np.arange(n)), list(idxs), list(ds))


def map_tss(ts, ts_):
    ps1 = get_points(ts)
    ps2 = get_points(ts_)

    m = {i: c.cid for i, c in enumerate(ts)}
    m_ = {i: c.cid for i, c in enumerate(ts_)}

    return {m[i]: m_[k]
            for i, k, _ in map_points(ps1, ps2)}


def th(P):
    n = np.shape(P)[0]
    P_ = np.ones((n, 4))

    P_[:, :-1] = P

    return P_


def fh(P):
    return P[:, :-1]


def tr_m(m: PolyData, T):
    m.points = fh(np.matmul(T, th(m.points).T).T)


def icp(source, target, trim=0.5, A=None, maxIter=1000, eps=0.005):
    N = np.shape(source)[0]

    for i in range(maxIter):
        print(i)
        source_tr = np.matmul(A, source.T).T
        phi = map_points(source_tr, target)

        print(N)

        trimN = int(np.floor(N*trim))

        phi_sorted = sorted(phi, key=dist)
        phi_tr = phi_sorted[:trimN]

        src_idxs = list(map(idx1, phi_tr))
        trg_idxs = list(map(idx2, phi_tr))

        A1, _, _, _ = lstsq(source_tr[src_idxs], target[trg_idxs])

        d = sum(list(map(dist,
                         map_points(np.matmul(A, source.T).T, target))))
        print(d)

        if d < eps:
            break
        else:
            A = A1

    return A


def project_ts(ts: STissue,  # projected on the cells here
               ts_: STissue,  # to merge
               phi: Dict[int, int]) -> STissue:  # phi is ts -> ts_
    nms = (list(list(ts.cells.values())[0].exprs.keys()) +
           list(list(ts_.cells.values())[0].exprs.keys()))
    cells_ = dict()
    for c in ts:
        c_ = ts_.cells[phi[c.cid]]
        cells_[c.cid] = Cell(c.cid,
                             c.pos,
                             c.vol,
                             union(c.exprs, c_.exprs))

    return STissue(cells_, dict(), geneNms=nms)


def select_points(ts):
    sx = sorted(list(ts), key=lambda c: c.pos.x)
    sy = sorted(list(ts), key=lambda c: c.pos.y)

    return np.array([sx[0].pos.toArray(), sx[-1].pos.toArray(),
                     sy[0].pos.toArray(), sy[-1].pos.toArray()])


# given ts, ts_
# ts__ = project_ts(ts, ts_, map_tss(ts, ts_))
