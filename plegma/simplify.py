import argparse  # type: ignore
import logging  # type: ignore
import plegma.mesh as mp  # type: ignore
import pyvista as pv  # type: ignore
import numpy as np  # type: ignore


def fix_mesh(m):
    m = mp.correct_bad_mesh(m)
    m = mp.ECFT(m, 0)

    m = m.compute_normals()
    
    if np.sum(m.point_normals, axis=0)[0] < 0:
        m.flip_normals()

    return m


def remesh(m, npoints, niter=1):
    m = m.decimate(0.95, volume_preservation=True,
                   normals=True, inplace=False)
    m = mp.ECFT(m, 0)

    m = mp.correct_bad_mesh(m)
    m = mp.ECFT(m, 0)

    for _ in range(niter):
        m = mp.remesh(m, npoints, holefill=0)
        fix_mesh(m)

    return m


def remesh1(m, tsize):
    m = m.compute_cell_sizes()
    n = int(np.round(np.sum(m.cell_arrays["Area"]) / tsize))
    print(n)

    return remesh(m, n)


def get_mesh(fname):
    return pv.PolyData(fname)


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--finput",
                        help="input filename (ply)",
                        required=True)
    parser.add_argument("-o", "--foutput",
                        help="output filename",
                        required=True)
    parser.add_argument("-r", "--res",
                        help="resolution (number of triangles)",
                        required=True)
    return parser


def command_line_runner():
    parser = get_parser()
    args = parser.parse_args()

    logging.info("Getting mesh from file..")
    m = get_mesh(args.finput)
    logging.info("Remeshing to " + str(args.res) + " triangles")
    m = remesh(m, int(args.res))

    m.save(args.foutput, binary=False)

    return


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.INFO)
    command_line_runner()
